context("test-method")

test_that("awesomemethod return values", {
  expect_equal(awesomemethod(), TRUE)
  expect_equal(awesomemethod(FALSE), FALSE)
  expect_equal(awesomemethod(TRUE), TRUE)


})
